import pytest
# Confirm that specific packages and versions are installed
@pytest.mark.parametrize("name", [
    ("docker")
])
def test_packages(host, name):
    pkg = host.package(name)
    assert pkg.is_installed
